package pe.com.core.test.unit.test;

import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import pe.com.core.dao.ProductoDao;

import pe.com.core.entity.Producto;

@RunWith(MockitoJUnitRunner.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ProductoTest {
	
	@Mock
	private ProductoDao productoDao;
	
	@Mock
	private Producto producto;
	
	@Test
	public void a_insertar() {
		try {
			System.out.println("Método Insertar");
			producto = new Producto();
			producto.setNombre("Fanta");
			producto.setIdProducto(1);
			producto.setIdCategoria(1);
			producto.setPrecio(1.00);
			when(productoDao.insertar(Matchers.any())).thenReturn(producto);
			Assert.assertTrue(producto.getIdProducto() > 0);
		} catch (Exception e) {
			e.printStackTrace();
            Assert.fail();
		}
	}
	
	@Test
	public void b_actualizar() {
		try {
			System.out.println("Método Actualizar");
            Producto productoBuscado;
            producto.setNombre("Fanta");
            producto.setIdProducto(1);
            when(productoDao.obtener(producto)).thenReturn(producto);
            productoBuscado = productoDao.obtener(producto);
            when(productoDao.actualizar(Matchers.any())).thenReturn(producto);
            productoDao.actualizar(producto);
            Assert.assertEquals(producto.getNombre(),productoBuscado.getNombre());
		} catch (Exception e) {
			e.printStackTrace();
            Assert.fail();
		}
	}
	
	@Test
    public void c_obtener() {
        try {
            System.out.println("Método Obtener");
            Producto productoBuscado;
            producto.setNombre("Fanta");
            producto.setIdProducto(1);
            when(productoDao.obtener(producto)).thenReturn(producto);
            productoBuscado = productoDao.obtener(producto);
            Assert.assertNotNull(productoBuscado);            
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail();
        }
    }
	
	@Test
    public void d_listar() {
        try {
            System.out.println("Método Listar");
            List<Producto> listaProductos = spy(new ArrayList<Producto>());
            when(listaProductos.add(Matchers.any())).thenReturn(true);
            when(productoDao.listar()).thenReturn(listaProductos);
            List<Producto> lista = productoDao.listar();
            Assert.assertTrue(lista.size() > 0);
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail();
        }
    }
	
	@Test
    public void e_eliminar() {
        try {
            System.out.println("Método Eliminar");
            when(productoDao.eliminar(producto)).thenReturn(producto);
            productoDao.eliminar(producto);
            Assert.assertTrue(true);
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail();
        }
    }
}
