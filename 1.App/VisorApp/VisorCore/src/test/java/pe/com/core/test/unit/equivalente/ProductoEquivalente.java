package pe.com.core.test.unit.equivalente;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import pe.com.core.dao.ProductoDao;
import pe.com.core.entity.Categoria;
import pe.com.core.entity.Producto;


@RunWith(MockitoJUnitRunner.class)
public class ProductoEquivalente {

	@Mock
	private ProductoDao productoDao;
	
	@Mock
	private Producto producto;
	
	
	@Test
	public void testCP01() {
		try {
			producto = new Producto();
			producto.setNombre("FANTA");
			producto.setIdProducto(1);
			producto.setIdCategoria(1);
			producto.setPrecio(3.00);
			when(productoDao.insertar(Matchers.any())).thenReturn(producto);
			Assert.assertTrue(producto.getIdProducto() > 0);
		} catch (Exception e) {
			e.printStackTrace();
            Assert.fail();
		}
	}
	
	@Test
	public void testCP02() {
		try {
			producto = new Producto();
			producto.setNombre("");
			producto.setIdProducto(1);
			producto.setIdCategoria(1);
			producto.setPrecio(null);
			when(productoDao.insertar(Matchers.any())).thenReturn(producto);
			Assert.assertTrue(producto.getIdProducto() > 0);
		} catch (Exception e) {
			e.printStackTrace();
            Assert.fail();
		}
	}
	
	@Test
	public void testCP03() {
		try {
			producto = new Producto();
			producto.setNombre(null);
			producto.setIdProducto(1);
			producto.setIdCategoria(1);
			producto.setPrecio(3.00);
			when(productoDao.insertar(Matchers.any())).thenReturn(producto);
			Assert.assertTrue(producto.getIdProducto() > 0);
		} catch (Exception e) {
			e.printStackTrace();
            Assert.fail();
		}
	}
	
	@Test
	public void testCP04() {
		try {
			producto = new Producto();
			producto.setNombre(null);
			producto.setIdProducto(1);
			producto.setIdCategoria(1);
			producto.setPrecio(null);
			when(productoDao.insertar(Matchers.any())).thenReturn(producto);
			Assert.assertTrue(producto.getIdProducto() > 0);
		} catch (Exception e) {
			e.printStackTrace();
            Assert.fail();
		}
	}
	
	
}
