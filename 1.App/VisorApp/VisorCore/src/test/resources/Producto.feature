Feature: Mantenimiento de Producto
  Como jefe de almacen necesito realizar la gestión de Productos
  
	Scenario: Registrar Producto
	    Given despues de iniciar sesion en la aplicacion
	    When 	hago click en el enlace de Mantenimiento de Producto
	    And 	luego hago click en el boton de Nuevo
	    And 	en la nueva pantalla escribo en campo Nombre el valor de "Fanta"
	    And 	en la nueva pantalla selecciono en campo Categoria el valor de "1" 
	    And 	en la nueva pantalla escribo en campo Precio el valor de "1.00"
	    And 	presiono el boton de Guardar
	    Then 	el sistema me muestra el mensaje de: "Se guardo correctamente el Producto"
    
	Scenario: Actualizar Producto
	    Given despues de iniciar sesion en la aplicacion
	    When 	hago click en el enlace de Mantenimiento de Producto
	    And 	busco el producto "Fanta" para seleccionarlo de la tabla de Productos
	    And 	luego hago click en el boton de Editar
	    And 	en la nueva pantalla escribo en campo Nombre el valor de "Crush"
	    And 	en la nueva pantalla selecciono en campo Categoria el valor de "1" 
	    And 	en la nueva pantalla escribo en campo Precio el valor de "1.20"
	    And 	presiono el boton de Actualizar
	    Then 	el sistema me muestra el mensaje de: "Se actualizo correctamente el Producto"
    
	Scenario: Eliminar Producto
	    Given despues de iniciar sesion en la aplicacion
	    When 	hago click en el enlace de Mantenimiento de Producto
	    And 	busco la categoria "Productos" para seleccionarlo de la tabla de Productos
	    And 	luego hago click en el boton de Eliminar
	    Then 	el sistema me muestra el mensaje de: "Se elimino correctamente el Producto"